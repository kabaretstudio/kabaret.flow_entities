# Changelog

## [1.0.0] - 2024-04-26

After 3 years of beta testing of version 0.0.1, this is the long awaited release of version 1.0.0 stable for production.

### Changed

issue #4: Verbose property print : removed annoying print and change it to logger

## [0.0.1] - 2021-03-30

Initial release by Damien Dee Coureau allowing to expand flow entities to Mongo

